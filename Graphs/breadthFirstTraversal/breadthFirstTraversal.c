#include<stdio.h>
#include<stdlib.h>
#include"queue.c"

#define MAX 5

struct vertex{
    char label;
    int visited;
};

struct vertex *lstVertices[MAX];
  
int adjMatrix[MAX][MAX];

int vertexCount = 0;


void addVertex(char label){
    struct vertex *vertex = (struct vertex *)malloc(sizeof(struct vertex));
    vertex->label = label;
    vertex->visited = 0;
    lstVertices[vertexCount++] = vertex;
};


void addEdge(int start, int end){
    adjMatrix[start][end] = 1;
    adjMatrix[end][start] = 1;
}


int unvisitedAdjMatrixVertex(int vertex){
    if(vertex == -1)
        return -1;
    for(int i=0; i<vertexCount; i++){
        if(adjMatrix[vertex][i] == 1 && lstVertices[i]->visited == 0){
            return i;
        }
    }
    return -1;
}


void displayVertex(int vertex){
    printf("%c", lstVertices[vertex]->label);
}


void breadthFirstTraversal(int vertex){
    struct Queue *queue = createQueue(MAX);
    lstVertices[0]->visited = 1;
    displayVertex(0);
    push(queue, 0);
    int unvisited;     
    while(!isEmpty(queue)){
        int tempVertex = pop(queue);

        while((unvisited = unvisitedAdjMatrixVertex(tempVertex))!= -1){
            lstVertices[unvisited]->visited = 1;
            displayVertex(unvisited);
            push(queue, unvisited);
        }
    }

}


int main(int argc, char **argv){
    if(argc != 1){
        fprintf(stderr, "Usage %s", argv[0]);
        return -1;
    }
    
    for(int i=0; i<MAX; i++){
        for(int j=0; j<MAX; j++){
            adjMatrix[i][j]=0;
        }
    } 

    addVertex('S');
    addVertex('A');
    addVertex('B');
    addVertex('C');
    addVertex('D');

    addEdge(0, 1);
    addEdge(0, 2);   
    addEdge(0, 3); 
    addEdge(1, 4);
    addEdge(2, 4);
    addEdge(3, 4);

    breadthFirstTraversal(0);
    return 0;
}

