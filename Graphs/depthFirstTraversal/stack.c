#include<stdio.h>
#include<stdlib.h>


struct Stack{
	int top;
	int capacity;
	int *data;
};


struct Stack *createStack(int capacity){
	struct Stack *stack = (struct Stack*)malloc(sizeof(struct Stack));
	stack->data = (int*)malloc(sizeof(int)*capacity);
	stack->top = -1;
	stack->capacity = capacity;
	return stack;
}

int isEmpty(struct Stack *stack){
	if(stack->top == -1){
		return 1;
	}
	return 0;
}


int isFull(struct Stack *stack){
	if(stack->top == (stack->capacity)-1){
		return 1;
	}else{
		return 0;
	}
}


int pop(struct Stack *stack){
	if(isEmpty(stack)==0){
		int data = stack->data[stack->top];
		stack->top -=1;
		return data;
	}
	printf("Stack is Empty\n");
	return -1;
}


int push(struct Stack *stack, int data){
	if(isFull(stack)==0){
		stack->top +=1;
		stack->data[stack->top] = data;
		return 1;
	}
	printf("Stack is Full\n");
}


int peek(struct Stack *stack){
    if(isEmpty(stack))
        return -1;

    int data = pop(stack);
    push(stack, data);
    return data;
}

/*
void main(int argc, char **argv){
	if(argc==2){
		printf("%s", argv[1]);
	}
	struct Stack *stack = createStack(40);
	printf("pushed successfully:%d\n",push(stack, 1));
	printf("pushed successfully:%d\n",push(stack, 2));
	printf("pushed successfully:%d\n",push(stack, 1));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("pushed successfully:%d\n",push(stack, 1));
	printf("pushed successfully:%d\n",push(stack, 2));
	printf("pushed successfully:%d\n",push(stack, 3));
	printf("%d\n",pop(stack));
	printf("pushed successfully:%d\n",push(stack, 2));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	
}
*/
