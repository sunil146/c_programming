#include<stdio.h>
#include<stdlib.h>
#include"stack.c"

#define MAX 5

struct vertex{
    char label;
    int visited;
};


struct vertex *lstVertices[MAX];

int vertexCount=0;

int adjMatrix[MAX][MAX];

void addVertex(char label){
    struct vertex *vertex = (struct vertex *)malloc(sizeof(struct vertex));
    vertex->label = label;
    vertex->visited = 0;
    lstVertices[vertexCount++] = vertex;
}


void addEdge(int start, int end){
    adjMatrix[start][end] = 1;
    adjMatrix[end][start] = 1;
}


int unvisitedAdjMatrixVertex(int vertex){
    if(vertex == -1)
        return -1;
    for(int i=0; i<vertexCount; i++){
        if(adjMatrix[vertex][i] == 1 && lstVertices[i]->visited == 0){
            return i;
        }
    }
    return -1;
}


void displayVertex(int vertex){
    printf("%c", lstVertices[vertex]->label);
}


int depthFirstTraversal(int vertex){
    struct Stack *stack = createStack(MAX);

    lstVertices[vertex]->visited = 1;
    displayVertex(vertex);
    push(stack, vertex);
    
    int unvisited;
    while(!isEmpty(stack)){
        
        unvisited = unvisitedAdjMatrixVertex(peek(stack));
        if(unvisited == -1){
            pop(stack);
        }else{
            lstVertices[unvisited]->visited = 1;
            displayVertex(unvisited);
            push(stack, unvisited);
        }
    }
}


int main(int argc, char **argv){
    if(argc != 1){
        fprintf(stderr, "Usage %s", argv[0]);
        return -1;
    }

    addVertex('S');
    addVertex('A');
    addVertex('B');
    addVertex('C');
    addVertex('D');
    
    addEdge(0,1);
    addEdge(0,2);
    addEdge(0,3);
    addEdge(1,4);
    addEdge(2,4);
    addEdge(3,4);
    
    depthFirstTraversal(0);

    return 0;
}
