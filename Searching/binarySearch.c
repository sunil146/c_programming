#include<stdio.h>
#include<stdlib.h>


int binarySearch(int arr[], int l, int h, int key){
    if(l <= h){
        int mid = (l+h)/2;
        
        if(arr[mid] == key){
            return mid;
        }

        if(key > arr[mid]){
            return binarySearch(arr, mid+1, h, key);
        }else{
            return binarySearch(arr, l, mid-1, key);
        }
    }
}


int main(int argc, char **argv){
    
    if(argc != 2){
        fprintf(stderr, "Usage %s -key", argv[0]);
        return -1;
    }

    int arr[] = {1,2,3,4,5,6,7,8,10,13,15};

    int result = binarySearch(arr, 0, 11, atoi(argv[1]));
    fprintf(stdout, "%d", result);

    return 0;
}
