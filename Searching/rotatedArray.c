/*This finds the element in the rotated sorted array,
 * the overall methodology is the same, just that you
 * have few more comparisons.
 */

#include<stdio.h>
#include<stdlib.h>


int search(int arr[], int l, int h, int key){

    if(l>h){ 
        return -1;
    }

    int mid = (l+h)/2;
    if(arr[mid] == key){ 
        return mid;
    }
    
    if(arr[l] <= arr[mid]){

        if(key >= arr[l] && key <= arr[mid]){
            return search(arr, l, mid-1, key);
        } 

        return search(arr, mid+1, h, key);
    }    
    
    if(key >= arr[mid] && key <= arr[h]){
        return search(arr, mid+1, h, key); 
        }
}


int main(int argc, char **argv){
    if(argc != 2){
        fprintf(stderr, "Usage %s -key", argv[0]);
        return -1;
    }
    int arr[] = {3,4,5,6,7,1,2,};
    int result = search(arr, 0, 6, atoi(argv[1]));

    fprintf(stdout, "%d", result);
    return 0;
}
