#include <stdio.h>
#include <stdlib.h>


void printElements(int *arr, int n){
	for(int i=0; i<n; i++){
		printf("%d ", arr[i]);
	}
	printf("\n");
}


void swap(int *a, int *b){
	int temp=*a;
	*a = *b;
	*b = temp;
}


void *quickSort(int *arr, int low, int high){
	if(low < high){	
		int i=low-1, j=low;	
		while(j <= high-1){
			if(arr[high] > arr[j]){
				i += 1;
				swap(arr+i, arr+j);
			}
			j+=1;
		}
		swap(arr+high, arr+i+1);
		
		quickSort(arr, low, i);
		quickSort(arr, i+2, high);	
	}
	printElements(arr, high+1);
}


int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array\n", argv[0]);
		exit(0);
	}

	int arr[argc-1];

	for(int i=0; i<argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}
	
	quickSort(arr, 0, argc-2);

	printElements(arr, argc-1);

	return 0;
}
