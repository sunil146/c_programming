#include <stdio.h>
#include <stdlib.h>

void selectionSort(int *arr, int count){
	int min_idx;

	for(int i=0; i < count -1; i++){
		min_idx = i;
		for(int j=i+1; j<count; j++){
			if(arr[min_idx] > arr[j]){
				min_idx = j;
			}
		}
		int temp = arr[i];
		arr[i] = arr[min_idx];
		arr[min_idx] = temp;
	}

	for(int i = 0 ; i<count; i++){
		printf("%d ", arr[i]);
	}
}


int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array\n", argv[0]);
		exit(0);
	}

	int arr[argc-1];

	for(int i=0; i<argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}
	
	selectionSort(arr, argc-1);

	return 0;
}
