#include <stdio.h>
#include <stdlib.h>


void printElements(int *arr, int n){
	for(int i=0; i<n; i++){
		printf("%d ", arr[i]);
	}
	printf("\n");
}


void insertionSort(int *arr, int n){
	int key, j;
	for(int i = 1; i<n; i++){
		key = arr[i];
		j = i - 1;
		while(j>= 0 && arr[j] > key){
			arr[j+1] = arr[j];	
			j -= 1;
		}
		arr[j+1] = key;
	}
}


int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array\n", argv[0]);
		exit(0);
	}

	int arr[argc-1];

	for(int i=0; i<argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}
	
	insertionSort(arr, argc-1);
	printElements(arr, argc-1);
	return 0;
}
