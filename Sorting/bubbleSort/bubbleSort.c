#include <stdio.h>
#include <stdlib.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}


void printElements(int *arr, int n){
	for(int i=0; i<n; i++){
		printf("%d ", arr[i]);
	}
	printf("\n");
}


void bubbleSort(int *arr, int n){
	int flag;
	for(int i = 0; i<n-1; i++){
		flag = 0;
		for(int j=0; j<n-i-1; j++){
			if(arr[j] > arr[j+1]){
                swap(arr+j, arr+j+1);
                flag += 1;
			}
			printElements(arr, n);
		}
		if(flag == 0){
			break;
		}
	}
}


int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array\n", argv[0]);
		exit(0);
	}

	int arr[argc-1];

	for(int i=0; i<argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}
	
	bubbleSort(arr, argc-1);    
    return 0;
}
