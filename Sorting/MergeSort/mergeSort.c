#include <stdio.h>
#include <stdlib.h>


void printElements(int *arr, int n){
	for(int i=0; i<n; i++){
		printf("%d ", arr[i]);
	}
	printf("\n");
}


int *merge(int *a, int num1, int *b, int num2){
	int i=0, j=0, count=0, *arr;
	arr = (int *)malloc(sizeof(int)*(num1+num2));
	while(i < num1 && j < num2){
		if(a[i] > b[j]){
			arr[count] = b[j];
			j += 1;
		}else{
			arr[count] = a[i];
			i += 1;
		}
		count += 1;
	}
	while(i < num1){
		arr[count] = a[i];
		i += 1;
		count += 1;
	}

	while(j < num2){
		arr[count] = b[j];
		j += 1;
		count += 1;
	}
	return arr;
}


int *mergeSort(int *arr, int n){
	int *a, *b;
	if(n == 1){
		int *temp = (int *)malloc(sizeof(int));
		*temp = arr[0];
		return temp;
	}else{
		a = mergeSort(arr, n/2);
		b = mergeSort(arr+n/2, n-n/2);
	}
	return merge(a, n/2, b, n-n/2);
}


int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array\n", argv[0]);
		exit(0);
	}

	int arr[argc-1];

	for(int i=0; i<argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}
	
	int *sorted = mergeSort(arr, argc-1);

	printElements(sorted, argc-1);

	return 0;
}
