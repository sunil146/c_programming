#include<stdio.h>
#include<stdlib.h>
#include<limits.h>

#define CAPACITY 4

struct Node{
    int data;
    int i; //index of array.
    int j; //index of the next element.
};

struct Node heap[CAPACITY];

int heapSize = 0;


int parent(int i){
    return (i-1)/2;
}


void swap(struct Node *a, struct Node *b){
    struct Node temp = *a;
    *a = *b;
    *b = temp;
}


void minHeapify(struct Node *arr, int n, int i){
    int left = 2*i+1;
    int right = 2*i+2;

    int smallest = i;
    
    if(left < n){
        if(arr[left].data < arr[smallest].data){
            smallest = left;
        }
    }

    if(right < n){
        if(arr[right].data < arr[smallest].data){
            smallest = right;
        }
    }

    if(smallest != i){
        swap(&arr[i], &arr[smallest]);
        minHeapify(arr, n, smallest);
    }
}


struct Node extractMin(struct Node *arr){
    if(heapSize <= 0){
        printf("Underflow");
        return;
    }

    if(heapSize == 1){
        heapSize--;
        return arr[0];
    }

    struct Node min = arr[0];
    arr[0] = arr[heapSize-1];
    heapSize--;
    minHeapify(arr, heapSize, 0);

    return min;
}


void insert(struct Node *arr, int data, int arrIdx, int idx){
    if(heapSize == CAPACITY){
        printf("Overflow");
    }

    heapSize++;
    int i = heapSize - 1; 
    struct Node node = {data, arrIdx, idx};

    arr[i]= node;
     
    while(i != 0 && arr[parent(i)].data > arr[i].data){
        swap(arr+i, arr+parent(i));
        i = parent(i);
    }
}


void printHeap(struct Node *arr){
    for(int i=0; i<heapSize; i++){
        printf("%d ", arr[i].data);
    }
}


int *mergeSortedArrays(int *arr[4], int rows, int columns, struct Node *heap){
    int i=0;
    while(i<CAPACITY){
        insert(heap, arr[i][0], i, 1);
        i++;
    }

    int *mergedArray = (int*)malloc(sizeof(int)*rows*columns);
    int count=0;
    while(count < rows*columns){
        struct Node min = extractMin(heap);
        mergedArray[count] = min.data; 
        
        //find the next element that'll replace current root of heap.
        if(min.j < 3){
            insert(heap, arr[min.i][min.j], min.i, min.j+1);
        }else{
            insert(heap, INT_MAX, min.i, min.j+1);
        }
        count++; 
    }
    
    return mergedArray;
}


int main(int argc, char **argv){
    if(argc < 2){
        fprintf(stderr, "Usage %s -sampleArr", argv[0]);
        return -1;
    } 
    int a[3] = {1,3,5};
    int b[3] = {2,3,5};
    int c[3] = {3,5,8};
    int d[3] = {2,4,7};


    int *arr[4] = {a, b, c, d};

    //mergeSortedArrays(arr, 3, heap);   
    /* int i = 0;
    while(i < CAPACITY){
        insert(heap, arr[i][0], i, 1);
        i++;
    }
    
    printHeap(heap);

    extractMin(heap);
    printf("\n"); 
    printHeap(heap);
    */

    int *result = mergeSortedArrays(arr, 4, 3, heap);
    
    for(int j = 0; j<12; j++){
        printf("%d ", result[j]);
    }

    return 0;
}
