/*This showcases the following:
 *1. inserting a node at the end of the linked list.
 *2. merging two sorted linked lists.
 *3. merging N linked lists.
 *4. Merge operation from the mid of the 'Merge Sort'.
 *
 */

#include<stdio.h>
#include<stdlib.h>


struct Node{
    int data;
    struct Node *next;
};


struct Node *insert(struct Node *head, int data){
    struct Node *n = (struct Node*)malloc(sizeof(struct Node));
    n->next = NULL;
    n->data = data;
    struct Node *curr = head;
    if(head == NULL){
        return n;
    }
    while(curr->next != NULL){
        curr = curr->next;
    }
    curr->next = n;
    return head;
}


struct Node *mergeTwoLists(struct Node *list1, struct Node *list2){
    struct Node *result = NULL;
    
    if(list1==NULL){
        return list2;
    }
    
    if(list2 == NULL){
        return list1;
    }

    if(list1->data < list2->data){
        result = list1;
        result->next = mergeTwoLists(list1->next, list2);
    }else{
        result = list2;
        result->next = mergeTwoLists(list1, list2->next);
    }
    return result;
}


struct Node *mergeMultipleLists(struct Node *list[], int last){
     
    while(last != 0){
        int i = 0, j = last;
        while(i < j){
            list[i] = mergeTwoLists(list[i], list[j]);
            i++;
            j--;
        }
        if(i >= j){
            last = j;
        }
   }
   return list[0];
}


void printList(struct Node *head){
    while(head!=NULL){
        printf("%d ", head->data);
        head = head->next;
    }
}


int main(int argc, char **argv){    
    struct Node *a=NULL, *b=NULL, *c=NULL, *d=NULL, *e=NULL;
    
    a = insert(a, 1);
    a = insert(a, 3);
    a = insert(a, 3);
    b = insert(b, 2);
    b = insert(b, 4);
    b = insert(b, 6); 
    c = insert(c, 3);
    c = insert(c, 5);
    c = insert(c, 7); 
    d = insert(d, 3);
    d = insert(d, 7);
    d = insert(d, 9);
    e = insert(e, 2);
    e = insert(e, 12);
    e = insert(e, 14);

    struct Node *lists[] = {a, b, c, d, e};
    
    //struct Node *result = mergeTwoLists(a, b);
    //printList(result);
    
    printf("\n");    
    
    struct Node *result1 = mergeMultipleLists(lists, 4);
    printList(result1);

    return 0;
}
