#include<stdio.h>
#include<stdlib.h>

struct Node{
    int data;
    struct Node *next;
};

struct Node *head;


void insertNode(int data){
    struct Node *n = (struct Node *)malloc(sizeof(struct Node));
    n->data = data;
    n->next = NULL;
    if(head == NULL){
        head = n;
        return;
    }
    struct Node *curr = head;
    while(curr->next != NULL){
        curr = curr->next;
    }
    curr->next = n;
}


void printList(){
    struct Node *curr = head;
    while(curr != NULL){
        printf("%d ", curr->data);
        curr = curr->next;
    }
}


int pallindrome(struct Node **headref, struct Node *curr){
    if(curr == NULL){
        return 1;
    }
    int pall;

    pall = pallindrome(headref, curr->next);

    if(pall == 0)
        return 0;
    
    if((*headref)->data == curr->data)
        pall = 1;
    else
        pall = 0;

    *headref = (*headref)->next;
    return pall;
}


int main(int argc, char **argv){
    if(argc != 1){
        fprintf(stderr, "Usage %s", argv[0]);
        return -1;
    }

    insertNode(1);
    insertNode(2);
    insertNode(3);
    insertNode(2);
    insertNode(1);
    
    printList();
    
    printf("\nChecking for Pallindrome: ");
    struct Node *curr = head;
    printf("%d ", pallindrome(&head, curr));

    return 0;

}
