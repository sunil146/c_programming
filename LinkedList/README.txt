/*This program showcases the following:
 * 1. Linked list creation
 * 2. Displaying the contents of the list;
 * 3. Finding if there is a loop in the list;
 * 4. Displaying the contents of the list in reverse using recursion.
 * 5. Reversing the list itself then printing it's contents;
*/
