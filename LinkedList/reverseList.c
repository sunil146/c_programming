/*This program showcases the following:
 * 1. Linked list creation
 * 2. Displaying the contents of the list;
 * 3. Finding if there is a loop in the list;
 * 4. Displaying the contents of the list in reverse using recursion.
 * 5. Reversing the list itself then printing it's contents;
*/
#include<stdio.h>
#include<stdlib.h>


struct node{
	int data;
	struct node *next;
};


struct node *createNode(int data){
	struct node *n = (struct node*)malloc(sizeof(struct node));
	n->data = data;
	n->next = NULL;
	return n;
}


void displayList(struct node *head){
	while(head->next != NULL){
		printf("%d ", head->data);
		head = head->next;
	}
	printf("%d\n", head->data);	
}


struct node *revList(struct node *head){
    struct node *curr = head, *next;
    while(curr->next != NULL){
        next = curr->next;
        curr->next = next->next;
        next->next = head;
        head = next;
    }
    return head;
}


struct node *reverseList(struct node *head){
	struct node * prev = NULL, *curr = head, *next = NULL;

	while(curr!=NULL){
		next = curr->next;
		curr->next = prev;
		prev = curr;
		curr = next;
	}
	return prev;

	/*
	struct node *prevNode = NULL;
	struct node *currNode = head;
	struct node *nextNode = head->next;
	
	
	while(nextNode->next!=NULL){
		currNode = nextNode;	
		nextNode = nextNode->next;
		currNode->next = prevNode;
		prevNode = currNode;
	}
	nextNode->next = currNode;
	return nextNode;
	*/
}

int findLoop(struct node *head){
	struct node *ptr1 = head;
	struct node *ptr2 = head;
	while((ptr1->next !=NULL) && (ptr2->next->next !=NULL)){
		ptr1 = ptr1->next;
		ptr2 = ptr2->next->next;
		if(ptr1->next == ptr2->next){
			printf("Loop Found");
			return 1;
		}
	}
	printf("Loop not Found\n");
	return 0;
}

void displayReverseList(struct node *head){
	if(head->next == NULL){
		printf("%d ", head->data);
		return;
	}else{
		displayReverseList(head->next);
		printf("%d ", head->data);
	}
}

void main(int argc, char **argv[]){
	struct node *head = createNode(1);
	struct node *second = createNode(2);
	struct node *third = createNode(3);
	struct node *fourth = createNode(4);
	struct node *fifth = createNode(5);
	struct node *sixth = createNode(6);
	struct node *seventh = createNode(7);
	struct node *a1 = createNode(8);
	struct node *a2 = createNode(9);
	struct node *a3 = createNode(10);
	struct node *a4 = createNode(11);
	struct node *a5 = createNode(12);
	struct node *a6 = createNode(13);
	struct node *a7 = createNode(14);
	
	head->next = second;
	second->next = third;
	third->next = fourth;
	fourth->next = fifth;
	fifth->next = sixth;
	sixth->next = seventh;
	seventh->next = a1;
	a1->next = a2;
	a2->next = a3;
	a3->next = a4;
	a4->next = a5;
	a5->next = a6;
	a6->next = a7;
	
	//Uncomment the next line to insert a loop in the linked list
	//a7->next = a7;
	
	int i = findLoop(head);
	if(i==0){
		printf("Printing the contents of the list\n");
		displayList(head);
		printf("printing list in reverse using recursion :\n");
		displayReverseList(head);
		printf("\nReversing the list itself and Printing the list:\n");
		head = reverseList(head);
		displayList(head);
        head = revList(head);
        displayList(head);
	}
	
}

//struct node *insertNode(struct node* root){
