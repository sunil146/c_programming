#include<stdio.h>
#include<stdlib.h>

#define swap(x,y) (x^=y^=x^=y)

struct temp{
    int a;
    char b;
    int c;
};


int gcd(int a, int b){
	while(a!=0 || b!=0){
		if(a == b){
			return a;
		}
		if(a%b == 0){
			return b;
		}
		if(b%a == 0){
			return a;
		}
		if(a > b){
			a = a%b;
		}else{
			b = b%a;
		}
	}
	return 0;
}


int main(int argc, char **argv){
	/*	
	if(argc !=3){
		fprintf(stderr, "Usage: %s int1 int2\n", argv[0]);
		return 1;
	}
	*/	
	//printf("%d", gcd(atoi(argv[1]), atoi(argv[2])));
		
	printf("%c", *(*(argv+0)+1));

	if(argc < 2){
		fprintf(stderr, "Usage: %s -arg\n", argv[0]);
	}

	int c;
	c = getchar();
	if(c != EOF) ungetc(c, stdin);
	
	c = getchar();
	printf("%c",c);

    struct temp a;
    printf("%d", sizeof(a));
    
    int x = 10, y = 2;
    swap(x,y);
    printf("%d %d", x, y);

	return 0;
}
