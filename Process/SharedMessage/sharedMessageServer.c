#include<stdio.h>
#include<sys/msg.h>
#include<sys/ipc.h>

struct msg_buffer{
	long msg_type;
	char msg_text[100];
};

int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -msg_type", argv[1]);
		exit(1);
	}

	key_t key;
	int msg_id;
	struct msg_buffer message;
	
	//ftok to generate unique key
	key = ftok("progfile", 65);

	//msgget creates a message queue and returns an identifier
	msg_id = msgget(key, 0666 | IPC_CREAT);
	message.msg_type = atoi(argv[1]);

	printf("Write data: ");
	gets(message.msg_text);

	//msgsnd to send message
	msgsnd(msg_id, &message, sizeof(message), 0);

	printf("Data sent is : %s \n", message.msg_text);

	//Testing if this works or not!
	
	
	return 0;
}
