This example demostrates the following:

1. Showcased how one can create a message queue.
2. Pass message to this queue using msgrcv()
3. Specify the message type using the msg_type in the struct msg_buffer.
4. Receive messages of specific msg_type if need be in the order we want.


