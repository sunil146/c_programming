#include<stdio.h>
#include<sys/ipc.h>
#include<sys/msg.h>

struct msg_buffer{
	long msg_type;
	char msg_text[100];
};

int main(int agrc, char **argv){

	key_t key;
	int msg_id;
	
	struct msg_buffer message;

	key = ftok("progfile", 66);
	msg_id = msgget(key, 0666| IPC_CREAT);
	

	//msgrcv to recieve a message
	msgrcv(msg_id, &message, sizeof(message), 1, 0);
	printf("Received message is : %s", message.msg_text);

	msgrcv(msg_id, &message, sizeof(message), 2, 0);
	printf("Received message is : %s", message.msg_text);	

	msgrcv(msg_id, &message, sizeof(message), 1, 0);
	printf("Received message is : %s", message.msg_text);	


	//to destroy message queue
	msgctl(msg_id, IPC_RMID, NULL);
	
	return 0;
}
