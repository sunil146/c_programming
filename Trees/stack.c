#include<stdio.h>
#include<stdlib.h>


struct Node {
	int data;
	struct Node *left, *right;
};


struct Stack{
	int top;
	int capacity;
	struct Node **arr;
};


struct Stack *createStack(int capacity){
	struct Stack *stack = (struct Stack*)malloc(sizeof(struct Stack));
	stack->arr = (struct Node**)malloc(sizeof(struct Node*) * capacity);
	stack->top = -1;
	stack->capacity = capacity;
	return stack;
}


int isEmpty(struct Stack *stack){
	if(stack->top == -1){
		return 1;
	}
	return 0;
}


int isFull(struct Stack *stack){
	if(stack->top == (stack->capacity)-1){
		return 1;
	}else{
		return 0;
	}
}


struct Node *peek(struct Stack *stack){
	if(!isEmpty(stack)){
		return stack->arr[stack->top];
	}
}


struct Node *pop(struct Stack *stack){
	if(isEmpty(stack)==0){
		struct Node *node = stack->arr[stack->top];
		stack->top -=1;
		return node;
	}
	printf("Stack is Empty\n");
	return;
}


int push(struct Stack *stack, struct Node *node){
	if(isFull(stack)==0){
		stack->top +=1;
		stack->arr[stack->top] = node;
		return 1;
	}
	printf("Stack is Full\n");
}

/*
void main(int argc, char **argv){
	if(argc>1){
		printf("%s", argv[0]);
	}
	struct Stack *stack = createStack(40);
	struct Node *n1 = (struct Node*)malloc(sizeof(struct Node));
	n1->data = 5;
	printf("pushed successfully:%d\n",push(stack, n1));
	struct Node *n = stack->arr[stack->top];
	printf("%d ",n->data);
	
	//need to change below lines to accomodate tree nodes
	printf("pushed successfully:%d\n",push(stack, 2));
	printf("pushed successfully:%d\n",push(stack, 1));
	printf("%d\n",pop(stack));
	printf("%d\n",pop(stack));
	printf("pushed successfully:%d\n",push(stack, 1));
	printf("pushed successfully:%d\n",push(stack, 2));
	printf("pushed successfully:%d\n",push(stack, 3));
}
*/
