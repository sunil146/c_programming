#include<stdio.h>
#include<stdlib.h>
#include "stack.c"

/*
struct Node{
	int data;
	struct Node *left, *right;
};
*/

struct Node *newNode(int data){
	struct Node *node = (struct Node *)malloc(sizeof(struct Node));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}


struct Node *insertNode(struct Node *root, int data){
	if(root == NULL){	
		root = newNode(data);
		return root;
	}else{
		if(root->data <= data){
			root->right = insertNode(root->right, data);
		}else{
			root->left = insertNode(root->left, data);
		}
	}
	return root;
}


void printPreOrder(struct Node *root){
	if(root){	
		printf("%d ", root->data);
		printPreOrder(root->left);
		printPreOrder(root->right);
	}
}


void printPostOrder(struct Node *root){
	if(root){
		printPostOrder(root->left);
		printPostOrder(root->right);	
		printf("%d ", root->data);
	}
}


void printInOrder(struct Node *root){
	if(root){
		printInOrder(root->left);
		printf("%d ", root->data);
		printInOrder(root->right);
	}
}


void iterPreOrder(struct Node *root){
	struct Stack *stack = createStack(3);
	push(stack, root);
	while(!isEmpty(stack)){
		struct Node *n = pop(stack);
		printf("%d ", n->data);
		if(n->right){
			push(stack, n->right);
		}
		if(n->left){
			push(stack, n->left);
		}
	}
}


void iterInOrder(struct Node *root){
	struct Node *curr = root;
	struct Stack *stack = createStack(5);		
	while(curr != NULL || !isEmpty(stack)){
		while(curr){
			//can change this to PreOrder by this
			//printf("%d ", curr->data);
			
			push(stack, curr);
			curr = curr->left;
		}
		struct Node *n = pop(stack);
		printf("%d ", n->data);
		curr = n->right;
	}
}


void iterPostOrder(struct Node *root){
	struct Stack *stack1 = createStack(10), *stack2 = createStack(10);
	push(stack1, root);
	while(!isEmpty(stack1)){
		struct Node *n = pop(stack1);
		push(stack2, n);
		if(n->left)
			push(stack1, n->left);
		if(n->right)
			push(stack1, n->right);
	}
	while(!isEmpty(stack2))
		printf("%d ", pop(stack2)->data);

}


void iterPostOrderUsingOneStack(struct Node *root){
	if(root == NULL) return;

	struct Stack *stack = createStack(10);
	struct Node *curr = root;
	do{
		while(curr != NULL){
			if(curr->right){
				push(stack, curr->right);
			}
			push(stack, curr);
			curr = curr->left;
		}
		
		curr = pop(stack);
		if(curr->right == peek(stack)){
			pop(stack);
			push(stack, curr);
			curr = curr->right;
		}else{
			printf("%d ", curr->data);
			curr = NULL;
		}
		
	}while(!isEmpty(stack));
}


int main(int argc, char **argv){
	if(argc != 1){
		fprintf(stderr, "Usage %s -array\n", argv[0]);
		return -1;
	}
	struct Node *root = NULL;
	
	int arr[7] = {4, 2, 1, 3, 6, 5, 7};

	for(int i=0; i<7; i++){
		root = insertNode(root, arr[i]);
	}

	printf("Using Recursion methods\n");
	printInOrder(root);
	printf("\n");
	printPreOrder(root);
	printf("\n");
	printPostOrder(root);
	printf("\n");
	
	printf("Using Non-recursive methods(stack usage)\n");
	//Non recursive approach using Stack
	iterPreOrder(root);	
	printf("\n");
	iterInOrder(root);
	printf("\n");
	iterPostOrder(root);
	printf("\n");
	iterPostOrderUsingOneStack(root);
	return 0;
}
