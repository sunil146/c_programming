//We can't modify the array given it's a pain, so hardcoded them.
//We use the function pointer to do the aescending or descending order branching
//by the use of a function pointer.


#include<stdio.h>
#include<stdlib.h>

struct Heap{
    int size;
    int *arr;
};

struct Heap *createHeap(int capacity){
    struct Heap *heap = (struct Heap *)malloc(sizeof(struct Heap));
    heap->arr = (int *)malloc(sizeof(int)*capacity);
    heap->size = capacity;
    return heap;
};


void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}


void heapify(int *arr, int n, int i){
    int left = 2*i+1;
    int right = 2*i+2;

    int largest = i;
    
    if(left < n){
        if(arr[left] > arr[largest]){
            largest = left;
        }
    }

    if(right < n){
        if(arr[right] > arr[largest]){
            largest = right;
        }
    }

    if(largest != i){
        swap(&arr[i], &arr[largest]);
        heapify(arr, n, largest);
    }
    
}


void heapify_min(int *arr, int n, int i){
    int left = 2*i+1;
    int right = 2*i+2;

    int smallest = i;
    
    if(left < n){
        if(arr[left] < arr[smallest]){
            smallest = left;
        }
    }

    if(right < n){
        if(arr[right] < arr[smallest]){
            smallest = right;
        }
    }

    if(smallest != i){
        swap(&arr[i], &arr[smallest]);
        heapify(arr, n, smallest);
    }
    
}

//this takes arr, size and the required sorting function pointer which would 
//we want i.e asending or descending order.
void heapSort(int *arr, int size, void (*ptr)(int *, int, int)){
    
    for(int i=size/2 - 1; i>= 0 ; i--){
        ptr(arr, size, i);
    }


    //sorting the array:
    for(int j = size - 1; j >= 0; j--){
        swap(arr, arr+j);
        ptr(arr, j, 0);
    }

}


int main(int argc, char **argv){
    if(argc != 3){
        fprintf(stderr, "Usage %s -cap -sortingType ('a' for asc, 'd' for des)\n", argv[0]);
        return -1;
    }

    struct Heap *heap = createHeap(atoi(argv[1]));
    heap->arr[0] = 10;
    heap->arr[1] = 4;
    heap->arr[2] = 3;
    heap->arr[3] = 5;
    heap->arr[4] = 1;
    heap->arr[5] = 7;
    heap->arr[6] = 4;
    
    void (*sort[2])(int *, int, int) = {heapify, heapify_min};

    if(*argv[2] == 'a'){
        heapSort(heap->arr, heap->size, sort[0]);
    }else{
        heapSort(heap->arr, heap->size, sort[1]);
    }
    
    for(int j = 0; j < heap->size; j++){
        printf("%d ", heap->arr[j]);
    }

    return 0;
}
