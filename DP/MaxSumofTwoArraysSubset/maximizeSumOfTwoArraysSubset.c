/*Given an array of integers(positive as well as negative) ,select some elements from this array(select a subset) such that:-
1. Sum of those elements is maximum(Sum of the subset is maximum) .
2. No 2 elements in the subset should be consecutive.
*/

#include <stdio.h>

int max(int a, int b){
	if(a > b){
		return a;
	}
	return b;
}

int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array", argv[0]);
		return -1;
	}

	int arr[argc-1];
	for(int i = 0; i < argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}

	//second array
	int arr1[4] = {-4, 2, 3, 40};

	int dp[argc-1];
	dp[0] = max(arr[0], arr1[0]);
	dp[1] = max(max(arr[1], arr[1]),dp[0]) ;

	for(int i=2; i<argc-1; i++){
		dp[i] = max(max(arr[i], arr1[i])+dp[i-2],dp[i-1]);
	}
	/*
	for(int i = 2; i < argc-1; i++){
		if(arr[i] > arr1[i]){
			if(dp[i-1] > arr[i] + dp[i-2]){
				dp[i] = dp[i-1];
			}else{
				dp[i] = arr[i] + dp[i-2];
			}
		}else{
			if(dp[i-1] > arr1[i] + dp[i-2]){
				dp[i] = dp[i-1];
			}else{
				dp[i] = arr1[i] + dp[i-2];
			}
		}
	}		
	*/
	for(int j=0; j < argc-1; j++){
		printf("%d ", dp[j]);
	}

	return 0;
}

