#include<stdio.h>

int max(int a, int b){
	if(a>b)return a;
	return b;
}


int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -number array", argv[0]);
		return -1;
	}

	int arr[argc-1], DP[argc-1];	
	for(int i = 0; i<argc-1; i++){
		arr[i] = atoi(argv[i+1]);
		DP[i] = 1;
	}

	for(int i=1; i<argc-1; i++){
		for(int j = 0; j<argc-2; j++){
			if(arr[j] < arr[i]){
				DP[i] = max(DP[i], DP[j]+1);
			}
		}
	}

	fprintf(stdout, "%d ", DP[argc-2]);
}


