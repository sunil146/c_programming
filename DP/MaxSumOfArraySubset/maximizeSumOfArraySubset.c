/*Given an array of integers(positive as well as negative) ,select some elements from this array(select a subset) such that:-
1. Sum of those elements is maximum(Sum of the subset is maximum) .
2. No 2 elements in the subset should be consecutive.
*/

#include <stdio.h>

int main(int argc, char **argv){
	if(argc < 2){
		fprintf(stderr, "Usage %s -array", argv[0]);
		return -1;
	}

	int arr[argc-1];
	for(int i = 0; i < argc-1; i++){
		arr[i] = atoi(argv[i+1]);
	}
	
	int dp[argc-1];
	dp[0] = arr[0];
	dp[1] = (arr[0] > arr[1]) ? arr[0]:arr[1];

	for(int i = 2; i < argc-1; i++){
		if(dp[i-1] > arr[i] + dp[i-2]){
			dp[i] = dp[i-1];
		}else{
			dp[i] = arr[i] + dp[i-2];
		}
	}

	for(int j=0; j < argc-1; j++){
		printf("%d ", dp[j]);
	}

	return 0;
}

