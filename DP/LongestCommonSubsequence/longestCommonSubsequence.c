#include<stdio.h>
#include<string.h>

int max(int a, int b){
	if(a>b)return a;
	return b;
}

int main(int argc, char **argv){
	if(argc < 3){
		fprintf(stderr, "Usage %s -string1 -string2",argv[0]);
		return -1;
	}

	char *str1 = argv[1];
	char *str2 = argv[2];
	//char *str1 = "abcdaf";
	//char *str2 = "acbcf";
	int len1 = strlen(str1);
	int len2 = strlen(str2);

	int DP[len2+1][len1+1];
	for(int j=0; j<len1+1; j++){
		DP[0][j] = 0;
	}
	for(int j=0; j<len2+1; j++){
		DP[j][0] = 0;
	}

	for(int i=1; i<=len2; i++){
		for(int j=1; j<=len1; j++){
			if(str1[j-1] == str2[i-1]){
				DP[i][j] = DP[i-1][j-1]+1;
			}else{
				DP[i][j] = max(DP[i-1][j], DP[i][j-1]);
			}
		}
	}
	printf("%d ", DP[len2][len1]);

}
