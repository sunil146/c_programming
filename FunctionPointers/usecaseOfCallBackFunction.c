#include<stdio.h>
//#include<math.h>
//we want to sort a given array in increasing order.
//what if we want to sort the same array in decreasing order too? 

//Call back function should compare two integers.
//It should return 1 if the first element has higher rank.
//0 if the elements are equal and -1 if the second element has higher rank.


void printElements(int *arr, int n){
     for(int i=0; i<n; i++){
         printf("%d ", arr[i]);
     }
     printf("\n");
}


//Generic function for comparison:
int compare_generic(const void *a, const void *b){
    int A = *((int*)a);
    int B = *((int*)b);
    return B-A; 
    //return abs(A) - abs(B); //to sort according to the absolute value.
}

//decreasing order comparision.
int compareDecreasing(int a, int b){
    if(a>b)return -1;
    else return 1;
}

//increasing order comparision.
int compareIncreasing(int a, int b){
    if(a>b)return 1;
    else return -1;
}


void bubbleSort(int *A, int n, int (*compare)(int, int)){
    int flag;
    for(int i=0; i<n-1; i++){
        flag = 0;
        for(int j=0; j<n-i-1; j++){
            if(compare(A[j], A[j+1])>0){
                int temp = A[j];
                A[j] = A[j+1];
                A[j+1] = temp;
                flag += 1;
            }
            printElements(A, n);
        }
        if(flag == 0)return;
    } 
}


int main(int argc, char **argv){
    
    int A[] = {3, 2, 1, 5, 6, 4};
    
//    void (*ptr)(int *, int, compare)=bubbleSort;
    
  //  ptr(A, 6);
    bubbleSort(A, 6, compareDecreasing);

    return 0;
}
