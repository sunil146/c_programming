#include<stdio.h>

//this is how you easily write a function pointer return type.
typedef void (*funcPtr)();


void Hello(){
    printf("Hello");
}


funcPtr test(void (*ptr)()){
    return ptr;
}


int main(int argc, char **argv){
    //defining a function pointer, then passing that function pointer.    
    void (*ptr)();
    ptr = test(Hello);
    ptr(); 
    printf("\n");
    
    //we can also simply do this:
    test(Hello);

}
