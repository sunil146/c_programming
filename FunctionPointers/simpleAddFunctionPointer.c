#include<stdio.h>

int add(int a, int b){
    return a+b;
}

int main(int argc, char **argv){
    
    int (*ptr)[10]; //pointer to an array.


    //function that has two arguments, int and int, that returns a integer value.
    int (*ptr1)(int, int);
  
    //we can also do the 'ptr1 = add'
    ptr1 = &add;
    int (*ptr2)(int, int) = add;

    //we can simply use 'result = ptr(10,20)' since we aren't using '&' in 'ptr1 = add'.
    int result = (*ptr1)(10,20);
    int result1 = ptr2(10,30);

    fprintf(stdout, "First result: %d\nSecond result: %d", result, result1);
    return 0;
}
