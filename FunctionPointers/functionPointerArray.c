#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

int add(int a, int b){
    return a+b;
}


int subtract(int a, int b){
    return a-b;
}


int multiply(int a, int b){
    return a*b;
}


int main(int argc, char **argv){
    if(argc != 2){
        fprintf(stderr, "Usage %s -arg (0 for add, 1 for subtract, 2 to multiply\n", argv[0]); 
        return -1;
    }
    
    int (*ptr[])(int, int) = {add, subtract, multiply};
    int temp = atoi(argv[1]);
    assert(temp < 3);

    fprintf(stdout, "%d", ptr[temp](20, 10));

    return 0;
} 
