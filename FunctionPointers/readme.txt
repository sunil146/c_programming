Use cases of function pointers:

1.  One of the use cases is showcased in the example: 'usecaseOfCallBackFunction.c'. 
    What we did in that is to instead of changing the logic of the actual sorting code,
    we simply use a callback function, specified by the function argument we pass, and 
    hence the function call only needs to be modified.

    So in a way, we may write our code logic for the compare function as our need requires,
    just pass that function pointer to the normal bubbleSort() which has the function pointer
    as an argument.
