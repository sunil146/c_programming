#include<stdio.h>

void Hello(){
    printf("Hello");
}

void Hellow(char *a){
    printf("Hello %s", a);
}

int main(int argc, char **argv){

    void (*ptr)() = Hello;

    //calling the function using pointer:
    ptr();

    void (*ptr1)(char *) = Hellow;
    
    char *a = "Sunil";
    //calling the function using pointer:
    printf("\n");
    ptr1(a);

    return 0;
}
