#include <stdio.h>
#include <stdlib.h>


struct Queue{
	unsigned capacity;
	int front, rear;
	int *array;
};


struct Queue *createQueue(unsigned capacity){
	struct Queue *queue = (struct Queue*)malloc(sizeof(struct Queue));
	queue->front = queue->rear = -1;
	queue->capacity = capacity;
	queue->array = (int*)malloc(sizeof(int)*queue->capacity);
	return queue;
}


int isFull(struct Queue *queue){
	if(queue != NULL){
		 if((queue->capacity + queue->rear - queue->front + 1)%queue->capacity == 0){
			 return 1;
		 }
		 return 0;
	}
	return -1;
}


int isEmpty(struct Queue *queue){
	if(queue != NULL){
		if((queue->rear == queue->front) && (queue->rear == -1)){
			return 1;
		}
		return 0;
	}
	return -1;
}


int pop(struct Queue *queue){
	if(queue != NULL){
		if(!isEmpty(queue)){
			int data;
			if(queue->front == queue->rear){
				data = queue->array[queue->front];
				queue->front = queue->rear = -1;
				return data;
			}else{
			data = queue->array[queue->front];
			queue->front = (queue->front+1)%queue->capacity;
			return data;
			}
		}
		return 0;
	}
	return -1;
}


int push(struct Queue *queue, int data){
	if(!isFull(queue)){
		if(isEmpty(queue)){
			queue->front = queue->rear = 0;
			queue->array[queue->rear] = data;
			return 1;
		}else{
		queue->rear = (queue->rear+1)%queue->capacity;
		queue->array[queue->rear] = data;
		return 1;
		}
	}
	return 0;
}


void main(int argc, char **argv){
	if(argc > 2){
		fprintf(stderr, "Usage %s\n", argv[0]);
		exit(0);
	}

	struct Queue *queue = createQueue(2);
	printf("%d\n", push(queue, 1));
	printf("%d\n", push(queue, 2));
	printf("%d\n", push(queue, 4));
	printf("%d\n", pop(queue));
	printf("%d\n", push(queue, 4));
	printf("%d\n", push(queue, 1));
	printf("%d\n", pop(queue));
	printf("%d\n", pop(queue));
	
	printf("just trying makefile");

}
