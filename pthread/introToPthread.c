#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>


long long sum = 0;

void *sum_runner(void* arg){
	long long limit = *(long long*)arg;
	
	for(long long i = 0; i <= limit; i++){
		sum += i;
	}
    // the below function would cause the thread to terminate.
	pthread_exit(0);
}
void main(int argc, char **argv){

	if(argc < 2){
		fprintf(stderr, "Usage %s -arg\n", argv[0]);
	}

	long long limit;

	limit = atoll(argv[1]);

	//pthread creation;
	pthread_t id;

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	pthread_create(&id, &attr, sum_runner, &limit);

    //this function will cause the calling thread to suspend until the thread given by 'id' is terminated.
	pthread_join(id, NULL);
	
	printf("Sum is %lld\n", sum);
}
