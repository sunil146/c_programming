#include<stdio.h>

//Simply uses a checker variable to store 26 characters a-z,
//and use bit manipulation to verify if any duplicate occurs.
int uniqueCharString(char *ptr){
	int checker = 0;
	int val;	
	for(int i=0; ptr[i]!='\0'; i++){
		val = ptr[i] - 'a';
		if((checker & (1 << val)) > 0){
			return 0;
		}
		checker |= (1 << val);
	}
	return 1;
}


int main(int argc, char **argv){
	if(argc < 2 && argc != 2){
		fprintf(stderr, "Usage %s -str\n", argv[0]);
		return -1;
	}
	
	fprintf(stdout, "%d ", uniqueCharString(argv[1]));
	
}
