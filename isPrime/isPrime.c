#include<stdio.h>
#include<stdlib.h>


int isPrime(int n){
	int factor;

	if(n%2 == 0){
		return n==2;
	}

	for(factor = 3; factor*factor <= n; factor+=2){
		if(n%factor==0){
			return 0;
		}
	}
	return 1;
}


int countPrime(int n){
	int i;
	int count;

	count =0;

	for(i = 0; i < n; i++){
		if(isPrime(i)) count++;
	}
	return count;
}


int main(int argc, char **argv){
	if(argc != 2){
		fprintf(stderr, "Usage: %s n\n", argv[0]);
		return 1;
	}

	printf("%d", countPrime(atoi(argv[1])));
	return 0;
}
