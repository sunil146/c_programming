You do the following when you're profiling the program using gprof.

$c99 -pg --static -g3 -o isPrime ./isPrime.c 

The profile is not very smart about shared libraries, we also including
the --static option to force the resulting program to be statically linked. This
means that all the code that is used by the program is baked into the executable
instead of being linked in at run-time. (Normally we don’t do this because
it makes for big executables and big running programs, since statically-linked
libraries can’t be shared between more than one running program.)

-pg option to gcc, which inserts profiling code that counts how many times each
function is called and how long(on average) each call takes.

Then we do this:

$time ./isPrime 100000

This produces a file called gmon.out which we can read with gprof.

We have to pass the name of the program so that gprof can figure out which executable
generated gmon.out

$gprof isPrime

Note: If we had used #include<math.h> then we would need to do the following:

Using the math library not only requires including <math.h> but also requires
compiling with the -lm flag after all .c or .o files, to link in the library routines:
$ c99 -pg --static -g3 -o countPrimes ./countPrimesSqrt.c -lm
$ time ./countPrimes 1000000
