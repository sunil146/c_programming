#include<stdio.h>

#define DEF_PAIR_OF(dtype) \
    typedef struct pair_of_##dtype { \
        dtype first; \
        dtype second; \
    } pair_of_##dtype##_t

DEF_PAIR_OF(int);
DEF_PAIR_OF(double);
DEF_PAIR_OF(char);

int main(int argc, char **argv){
    
   pair_of_int_t s;
   s.first = 10;
   s.second = 20;
    
   pair_of_char_t t;
   t.first = 'A';
   t.second = 'B';

   pair_of_double_t u;
   u.first = 10.23;
   u.second = 23.10;

   printf("%d %d", s.first, s.second);
   printf("%c %c", t.first, t.second);
   printf("%f %f", u.first, u.second);

   return 0;

}
