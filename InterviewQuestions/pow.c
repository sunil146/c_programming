#include<stdio.h>


float powFunc(float a, int b){
    int negFlag;
    if(b < 0){
        negFlag = 1;
        b = -b;
    }
    if(b == 0){
        return 1;
    }
    if(b == 1){
        return a;
    }
    if(b == 2){
        if(negFlag == 1){
            return -a * a;
        }
        return a * a;
    }
    
    float result; 
    if(b % 2 == 0){
        result = powFunc(a, b/2) * powFunc(a, b/2);
    }else{
        result = a * powFunc(a, b-1);
    }

    if(negFlag == 1){ 
        return 1.0/result;
    }

    return result;
}

int main(int argc, char **argv){
    printf("%f\n", powFunc(2, -2));
    printf("%f\n", powFunc(3.3, 4));
    printf("%f\n", powFunc(2, -4));
    printf("%f\n", powFunc(0, 4));
    printf("%f\n", powFunc(3, 0));
    printf("%f\n", powFunc(3, -4));
    printf("%f\n", powFunc(-3, 4));
    return 0;
}

